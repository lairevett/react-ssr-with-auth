const webpack = require('webpack');
const path = require('path');
const {BundleAnalyzerPlugin} = require('webpack-bundle-analyzer');
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const BrotliPlugin = require('brotli-webpack-plugin');
const TerserJSPlugin = require('terser-webpack-plugin');
const HtmlPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const PreloadPlugin = require('preload-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

const MODE = process.env.NODE_ENV || 'development';

const plugins = [
    new FriendlyErrorsPlugin(),
    new HtmlPlugin({
        filename: 'index.html',
        template: 'assets/index.html',
    }),
    new MiniCssExtractPlugin({
        filename: `static/css/${MODE === 'production' ? '[name].[hash]' : '[name]'}.css`,
    }),
    new PreloadPlugin({
        rel: 'preload',
        include: 'allAssets',
        fileBlacklist: [/\.txt/],
    }),
    new CompressionPlugin({
        filename: '[path].gz[query]',
        algorithm: 'gzip',
        test: /\.(css|js)$/,
        threshold: 10240,
        minRatio: 0.8,
    }),
    new BrotliPlugin({
        asset: '[path].br[query]',
        test: /\.(css|js)$/,
        threshold: 10240,
        minRatio: 0.8,
    }),
    new webpack.DefinePlugin({
        'process.env': {
            __CLIENT__: 'true',
            API_URL: `'${process.env.API_URL}'`,
        },
    }),
];

if (MODE === 'production') {
    plugins.push(
        new BundleAnalyzerPlugin({
            analyzerMode: 'static',
            reportFilename: 'webpack-report.html',
            openAnalyzer: false,
        })
    );
}

module.exports = {
    mode: MODE,
    cache: true,
    context: path.join(__dirname, 'src/__client__'),
    devtool: MODE === 'development' && 'source-map',
    entry: {
        app: './index.jsx',
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: /node_modules/,
                    name: 'vendor',
                    chunks: 'all',
                },
            },
        },
        minimizer: [new TerserJSPlugin({}), new OptimizeCSSAssetsPlugin({})],
    },
    resolve: {
        modules: [path.resolve(__dirname, 'src/'), 'node_modules'],
        extensions: ['.js', '.jsx'],
    },
    module: {
        rules: [
            {
                test: /\.woff2$/i,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'static/fonts/',
                        },
                    },
                ],
            },
            {
                test: /\.css$/i,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            hmr: MODE === 'development',
                        },
                    },
                    'css-loader',
                ],
            },
            {
                test: /\.(js|jsx|ts|tsx)$/,
                exclude: /(node_modules|src\/__client__\/assets)/,
                loader: 'babel-loader',
            },
        ],
    },
    output: {
        path: path.resolve(__dirname, 'public'),
        publicPath: '/',
        filename: `static/js/${MODE === 'production' ? '[name].bundle.[hash]' : '[name].bundle'}.js`,
    },
    plugins,
};
