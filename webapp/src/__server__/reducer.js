import {ACTION, INITIAL_STATE} from './constants';

const __server__Reducer = (state = INITIAL_STATE, action) => {
    if (action.type === ACTION.SET_WILL_DISPATCH_NEXT_EVENTS) {
        return {
            ...state,
            willDispatchNextEvents: action.payload,
        };
    }

    return state;
};

export default __server__Reducer;
