/* eslint-disable security/detect-non-literal-fs-filename */
import cors from 'cors';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import express from 'express';
import expressStaticGzip from 'express-static-gzip'; // eslint-disable-line import/no-extraneous-dependencies
import fs from 'fs';
import path from 'path';
import {handleRequest} from './utils/request';
import {makeRoomForSocket, getStreamInfo} from './utils/stream';
import {createPIDFile} from './utils/pid';
import handleMultipartFormData from './middleware/handle_multipart_form_data';
import createPipelineData from './middleware/create_pipeline_data';
import makeReduxStore from './middleware/make_redux_store';
import pingAPI from './middleware/ping_api';
import engageMatcher from './middleware/engage_matcher';
import forwardCSRFToken from './middleware/forward_csrf_token';
import retrieveUserData from './middleware/retrieve_user_data';
import {handleViewEvents} from './middleware/handle_view_events';
import {retrieveUrls} from '../app/utils/router';
import '../app/utils/urls';

const isProduction = process.env?.NODE_ENV === 'production';

export const createMaster = () => {
    if (isProduction) createPIDFile();
    const streamInfo = getStreamInfo();

    if (streamInfo[0] === 'socket') {
        makeRoomForSocket(streamInfo[1]);
    }
};

export const createSlave = () => {
    const streamInfo = getStreamInfo();
    const slave = express();
    slave.disable('x-powered-by'); // Don't send X-Powered-By: Express header.

    if (!isProduction) {
        // Serve static files from express when in development mode.
        // Static files in production are being served by nginx.
        const root = path.resolve(__dirname, '../../public');
        // noinspection JSCheckFunctionSignatures
        slave.use('/', expressStaticGzip(root, {index: false, enableBrotli: true}));
    }

    [
        cors(),
        cookieParser(),
        bodyParser.json(),
        bodyParser.urlencoded({extended: true}),
        createPipelineData,
        makeReduxStore,
        pingAPI,
        forwardCSRFToken,
        retrieveUserData,
        engageMatcher,
    ].forEach(middleware => slave.use(middleware));

    // For automatic colon params parsing in views.
    Object.values(retrieveUrls()).forEach(url => {
        if (url.methods.includes('get')) {
            // noinspection JSUnresolvedFunction
            slave.get(url.path, handleViewEvents, handleRequest);
        }

        if (url.methods.includes('post')) {
            // noinspection JSUnresolvedFunction
            slave.post(url.path, handleMultipartFormData(url), handleViewEvents, handleRequest);
        }
    });

    // noinspection JSUnresolvedFunction
    slave.get('*', handleViewEvents, handleRequest);

    slave.listen(streamInfo[1], () => {
        // SocketMode=0660 in .socket file is ignored resulting in 0755 mode,
        // www-data group needs to have write access, though.
        if (streamInfo[0] === 'socket') {
            fs.stat(streamInfo[1], (error, stats) => {
                if (error) {
                    throw error;
                }

                // eslint-disable-next-line no-bitwise
                if (+(stats.mode & 0o777).toString(8) !== 660) {
                    // eslint-disable-next-line no-shadow
                    fs.chmod(streamInfo[1], 0o660, error => {
                        if (error) {
                            throw error;
                        }
                    });
                }
            });
        }

        console.log(`Listening at ${streamInfo[0]} ${streamInfo[1]}...`);
    });
};
