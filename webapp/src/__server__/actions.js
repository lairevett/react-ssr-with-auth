import {ACTION, ENDPOINT} from './constants';
import {apiGetRequest} from '../app/utils/api/requests';

export const ping = () => apiGetRequest(ACTION.PING, {}, ENDPOINT.PING, {});

const setWillDispatchNextEvents = payload => ({type: ACTION.SET_WILL_DISPATCH_NEXT_EVENTS, payload});

// The zeroth dispatch is done server-side in middleware,
// this stops redundant client dispatch after loading the javascript.
export const makeClientHandleNextDispatches = (() => {
    let callCount = 0;

    return () => dispatch => {
        if (callCount === 1) {
            // @__server__/WILL_DISPATCH_NEXT_EVENTS false
            dispatch(setWillDispatchNextEvents(false));
        }

        callCount += 1;
    };
})();
