require('../../babel_loader');
const cluster = require('cluster');
const app = require('./app');

(async () => {
    if (cluster.isMaster) {
        await app.createMaster();
        const WORKERS = +process.env.WORKERS || 3;

        for (let i = 0; i < WORKERS; i++) {
            cluster.fork();
        }

        cluster.on('online', worker => {
            console.log(`Worker started (${worker.process.pid}).`);
        });

        cluster.on('exit', (worker, code, signal) => {
            console.log(`Worker ${worker.process.pid} died with code ${code} and signal ${signal}.`);
            cluster.fork();
        });
    } else {
        app.createSlave();
    }
})();
