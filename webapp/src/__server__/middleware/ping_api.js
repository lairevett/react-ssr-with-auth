import {ping} from '../actions';
import {STATUS} from '../../app/utils/api/constants';
import {makeErrorResponse} from '../utils/responses';
import {logDispatchAPIErrors} from '../../app/utils/logger';
import {APP_CSRF_COOKIE_NAME} from '../../app/constants';

const pingAPI = async (request, response, next) => {
    try {
        const pong = request.__PIPELINE_DATA__.store.dispatch(await ping());

        if (pong.payload.__meta__.status !== STATUS.OK) {
            return makeErrorResponse(response, 500, "API didn't respond with 200 OK on ping.");
        }

        // eslint-disable-next-line security/detect-object-injection
        request.__PIPELINE_DATA__.csrfToken = pong.context.setCookieHeaders[APP_CSRF_COOKIE_NAME].value;

        next();
    } catch (error) {
        logDispatchAPIErrors(error);
        return makeErrorResponse(response, 500, 'Failed to ping API.');
    }
};

export default pingAPI;
