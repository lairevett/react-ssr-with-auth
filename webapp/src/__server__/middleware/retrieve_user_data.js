import {APP_AUTH_COOKIE_NAME} from '../../app/constants';
import {setSignInOut, retrieveDetails as retrieveUserDetails} from '../../user/actions';
import {STATUS} from '../../app/utils/api/constants';
import {logDispatchAPIErrors} from '../../app/utils/logger';
import {makeErrorResponse} from '../utils/responses';

const retrieveUserData = async (request, response, next) => {
    try {
        const authToken = request.cookies?.[APP_AUTH_COOKIE_NAME];

        if (authToken) {
            const {dispatch} = request.__PIPELINE_DATA__.store;

            const {
                payload: {__meta__},
            } = dispatch(await retrieveUserDetails({headers: request.headers}));

            dispatch(setSignInOut(__meta__.status === STATUS.OK, {__meta__}));
        }

        next();
    } catch (error) {
        logDispatchAPIErrors(error);
        return makeErrorResponse(response, 500, 'Failed to retrieve user data.');
    }
};

export default retrieveUserData;
