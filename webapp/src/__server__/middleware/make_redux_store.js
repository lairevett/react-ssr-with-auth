import makeStore from '../../app/utils/store';
import {logError} from '../../app/utils/logger';
import {makeErrorResponse} from '../utils/responses';

const makeReduxStore = (request, response, next) => {
    try {
        request.__PIPELINE_DATA__.store = makeStore();

        next();
    } catch (error) {
        logError(error);
        return makeErrorResponse(response, 500, 'Failed to create store.');
    }
};

export default makeReduxStore;
