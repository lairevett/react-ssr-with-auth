import fs from 'fs';
import path from 'path';
import {makeSuccessResponse} from './responses';
import {render} from './renderer';

export const handleRequest = (request, response) => {
    const indexDirectory = path.join(__dirname, '/../../../public/');
    const indexPath = path.resolve(indexDirectory, 'index.html');
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    const indexContent = fs.readFileSync(indexPath, 'utf-8');

    const rendererData = render(request, response, indexContent);
    return makeSuccessResponse(response, rendererData);
};
