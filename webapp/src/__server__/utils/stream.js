/* eslint-disable security/detect-non-literal-fs-filename */
import fs from 'fs';

export const makeRoomForSocket = path => {
    fs.stat(path, (error, stats) => {
        if (error) {
            throw error;
        } else if (stats.isSocket()) {
            console.log('Provided socket address is already in use, trying to release it...');
            // noinspection JSCheckFunctionSignatures
            fs.unlink(path, () => {
                console.info('Socket address released successfully.');
            });
        }
    });
};

export const getStreamInfo = () => {
    const {UNIX_SOCKET} = process.env;
    const PORT = +process.env.PORT || 3000;
    const streamType = UNIX_SOCKET ? 'socket' : 'port';

    return [streamType, UNIX_SOCKET || PORT];
};
