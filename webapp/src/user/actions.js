import {apiDeleteRequest, apiGetRequest, apiPostRequest} from '../app/utils/api/requests';
import {ACTION, ENDPOINT, SIGN_IN_OUT_INITIAL_STATE, DETAILS_INITIAL_STATE} from './constants';
import {DEFAULT_STATUS_INITIAL_STATE} from '../app/utils/api/constants';

export const signUp = (username, email, password, context) =>
    apiPostRequest(
        ACTION.SIGN_UP,
        DEFAULT_STATUS_INITIAL_STATE,
        ENDPOINT.SIGN_UP,
        {username, email, password},
        context
    );

export const signIn = (username, password, context) =>
    apiPostRequest(ACTION.SIGN_IN, SIGN_IN_OUT_INITIAL_STATE, ENDPOINT.SIGN_IN, {username, password}, context);

export const signOut = context =>
    apiDeleteRequest(ACTION.SIGN_OUT, SIGN_IN_OUT_INITIAL_STATE, ENDPOINT.SIGN_OUT, context);

export const setSignInOut = (isAuthenticated, additionalPayload) => ({
    type: ACTION.SET_SIGN_IN_OUT,
    payload: {is_authenticated: isAuthenticated, ...additionalPayload},
});

export const retrieveDetails = context =>
    apiGetRequest(ACTION.RETRIEVE_DETAILS, DETAILS_INITIAL_STATE, ENDPOINT.RETRIEVE_DETAILS, context);

export const clearDetails = () => ({type: ACTION.CLEAR_DETAILS});
