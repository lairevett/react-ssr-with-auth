import {DEFAULT_STATUS_INITIAL_STATE} from '../app/utils/api/constants';

// API endpoints.
export const ENDPOINT = {
    SIGN_UP: '/api/users/sign-up/',
    SIGN_IN: '/api/users/sign-in/',
    SIGN_OUT: '/api/users/sign-out/',
    RETRIEVE_DETAILS: '/api/users/retrieve-user/',
};

// Redux action types.
export const ACTION = {
    SIGN_UP: '@user/SIGN_UP',
    SIGN_IN: '@user/SIGN_IN',
    SET_SIGN_IN_OUT: '@user/SET_SIGN_IN_OUT',
    SIGN_OUT: '@user/SIGN_OUT',
    RETRIEVE_DETAILS: '@user/RETRIEVE_DETAILS',
    CLEAR_DETAILS: '@user/CLEAR_DETAILS',
};

// Redux form names.
export const FORM = {
    SIGN_UP: 'FORM_USER_SIGN_UP',
    SIGN_IN: 'FORM_USER_SIGN_IN',
    SIGN_OUT: 'FORM_USER_SIGN_OUT',
};

// Reducer initial states.
export const SIGN_IN_OUT_INITIAL_STATE = {
    is_authenticated: false,
    ...DEFAULT_STATUS_INITIAL_STATE,
};

// Reducer initial states.
export const DETAILS_INITIAL_STATE = {
    id: 0,
    last_login: '',
    is_superuser: false,
    username: '',
    first_name: '',
    last_name: '',
    email: '',
    is_staff: false,
    is_active: true,
    date_joined: '',
    groups: [],
    user_permissions: [],
    ...DEFAULT_STATUS_INITIAL_STATE,
};

export const INITIAL_STATE = {
    signUp: DEFAULT_STATUS_INITIAL_STATE,
    signInOut: SIGN_IN_OUT_INITIAL_STATE,
    details: DETAILS_INITIAL_STATE,
};

// Prop types.
// https://i.kym-cdn.com/photos/images/newsfeed/001/488/679/e2d.jpg
// First frame - prop types, second frame - property types.
export const DETAILS_PROP_TYPES = PropertyTypes =>
    PropertyTypes.exact({
        id: PropertyTypes.number.isRequired,
        last_login: PropertyTypes.string,
        is_superuser: PropertyTypes.bool.isRequired,
        username: PropertyTypes.string.isRequired,
        first_name: PropertyTypes.string.isRequired,
        last_name: PropertyTypes.string.isRequired,
        email: PropertyTypes.string.isRequired,
        is_staff: PropertyTypes.bool.isRequired,
        is_active: PropertyTypes.bool.isRequired,
        date_joined: PropertyTypes.string.isRequired,
        groups: PropertyTypes.array.isRequired,
        user_permissions: PropertyTypes.array.isRequired,
    }).isRequired;
