import {ACTION, INITIAL_STATE, SIGN_IN_OUT_INITIAL_STATE} from './constants';

const userReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ACTION.SIGN_UP:
            return {
                ...state,
                signUp: action.payload,
            };
        case ACTION.SIGN_IN:
        case ACTION.SET_SIGN_IN_OUT:
            return {
                ...state,
                signInOut: action.payload,
            };
        case ACTION.SIGN_OUT:
            return {
                ...state,
                signInOut: SIGN_IN_OUT_INITIAL_STATE,
            };
        case ACTION.RETRIEVE_DETAILS:
            return {
                ...state,
                details: action.payload,
            };
        case ACTION.CLEAR_DETAILS:
            return INITIAL_STATE;
        default:
            return state;
    }
};

export default userReducer;
