import PropTypes from 'prop-types';
import React from 'react';
import UserSignInForm from '../forms/sign_in';

const UserSignIn = ({isError}) => <UserSignInForm isError={isError} />;

UserSignIn.propTypes = {
    isError: PropTypes.bool.isRequired,
};

export default UserSignIn;
