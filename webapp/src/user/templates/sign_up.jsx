import PropTypes from 'prop-types';
import React from 'react';
import UserSignUpForm from '../forms/sign_up';

const UserSignUp = ({isSuccess, isError}) => (
    <UserSignUpForm isSuccess={isSuccess} isError={isError} />
);

UserSignUp.propTypes = {
    isSuccess: PropTypes.bool.isRequired,
    isError: PropTypes.bool.isRequired,
};

export default UserSignUp;
