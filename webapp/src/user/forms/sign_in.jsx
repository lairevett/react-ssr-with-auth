import {Field, Form, reduxForm, propTypes} from 'redux-form';
import React from 'react';
import PropTypes from 'prop-types';
import {FormCard, FormField} from '../../app/templates/__partials__/form';
import {submit, validate} from './process_sign_in';
import ErrorAlert from '../../app/templates/__partials__/error_alert';
import {FORM} from '../constants';
import SubmitButton from '../../app/templates/__partials__/submit_button';

const UserSignInForm = ({handleSubmit, valid, pristine, submitting, submitFailed, isError}) => (
    <FormCard>
        {(submitFailed || isError) && <ErrorAlert message="Invalid credentials." />}
        <Form method="post" action="?" className="form" onSubmit={handleSubmit(submit)}>
            <Field
                id="username"
                name="username"
                type="text"
                label="Username"
                component={FormField}
                aria-labelledby="username"
            />
            <Field
                id="password"
                name="password"
                type="password"
                label="Password"
                component={FormField}
                aria-labelledby="password"
            />
            <SubmitButton
                className="mt-3"
                value="Sign in"
                disabled={!valid || pristine || submitting}
                aria-label="Sign in button"
            />
        </Form>
    </FormCard>
);

UserSignInForm.propTypes = {
    ...propTypes,
    isError: PropTypes.bool.isRequired,
};

export default reduxForm({
    form: FORM.SIGN_IN,
    enableReinitialize: true,
    validate,
    shouldValidate: () => process.env.__CLIENT__,
})(UserSignInForm);
