import React from 'react';
import {Form, reduxForm, propTypes} from 'redux-form';
import {submit} from './process_sign_out';
import {FORM} from '../constants';
import {retrieveUrl} from '../../app/utils/router';

const UserSignOutForm = ({handleSubmit}) => (
    <Form method="post" action={retrieveUrl('userSignOut').path} onSubmit={handleSubmit(submit)}>
        <button className="dropdown-item" type="submit">
            Sign out
        </button>
    </Form>
);

UserSignOutForm.propTypes = propTypes;

export default reduxForm({form: FORM.SIGN_OUT})(UserSignOutForm);
