import {Field, Form, reduxForm, propTypes} from 'redux-form';
import {Link} from 'react-router-dom';
import React from 'react';
import PropTypes from 'prop-types';
import {FormCard, FormField} from '../../app/templates/__partials__/form';
import {submit, validate} from './process_sign_up';
import ErrorAlert from '../../app/templates/__partials__/error_alert';
import {FORM} from '../constants';
import SubmitButton from '../../app/templates/__partials__/submit_button';
import SuccessAlert from '../../app/templates/__partials__/success_alert';
import {retrieveUrl} from '../../app/utils/router';

const UserSignUpForm = ({handleSubmit, valid, pristine, submitting, submitSucceeded, isSuccess, isError}) => {
    // Can't figure out how to unit test captcha, so I just get rid of the whole field when in test mode.
    // It was created to bypass robots in the end, right?
    // const isTestMode = process?.env?.JEST_WORKER_ID !== 'undefined';

    return (
        <FormCard>
            {(submitSucceeded || isSuccess) && (
                <SuccessAlert>
                    Account created successfully, you can <Link to={retrieveUrl('userSignIn').path}>sign in</Link> now.
                </SuccessAlert>
            )}
            {isError && <ErrorAlert />}
            <Form method="post" action="?" onSubmit={handleSubmit(submit)}>
                <Field
                    id="username"
                    name="username"
                    type="text"
                    label="Username"
                    component={FormField}
                    aria-labelledby="username"
                />
                <Field
                    id="email"
                    name="email"
                    type="email"
                    label="Email address"
                    component={FormField}
                    aria-labelledby="email"
                />
                <Field
                    id="password"
                    name="password"
                    type="password"
                    label="Password"
                    component={FormField}
                    aria-labelledby="password"
                />
                <Field
                    id="rules"
                    name="rules"
                    type="checkbox"
                    label="I accept the terms of service."
                    component={FormField}
                    parse={value => !!value}
                    aria-labelledby="rules"
                />
                {/* isTestMode && (
                    <Field id="recaptcha" name="recaptcha" component={ReCAPTCHA} aria-label="I'm not a robot captcha." />
                ) */}
                <SubmitButton
                    className="mt-3"
                    value="Sign up"
                    disabled={!valid || pristine || submitting}
                    noScriptDisabled
                    aria-label="Sign up button"
                />
            </Form>
        </FormCard>
    );
};

UserSignUpForm.propTypes = {
    ...propTypes,
    isSuccess: PropTypes.bool.isRequired,
    isError: PropTypes.bool.isRequired,
};

export default reduxForm({form: FORM.SIGN_UP, validate, shouldValidate: () => process.env.__CLIENT__})(UserSignUpForm);
