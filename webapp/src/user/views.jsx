import React from 'react';
import {Redirect} from 'react-router';
import UserSignUp from './templates/sign_up';
import UserSignIn from './templates/sign_in';
import {submitServer as submitServerSignUp} from './forms/process_sign_up';
import {submitServer as submitServerSignIn} from './forms/process_sign_in';
import {submitServer as submitServerSignOut} from './forms/process_sign_out';
import Content from '../app/templates/__partials__/content';
import Loading from '../app/templates/__partials__/loading';
import Suspense from '../app/utils/api/suspense';

/*
 * GET /profile
 */
export const UserProfileView = ({title, state}) => {
    // <Suspense status={} fallback={<Loading />}>... not needed here,
    // user info is fetched by the express middleware anyway.
    const {details} = state.user;

    return (
        <Content title={title}>
            <ul>
                <li>Username: {details.username}</li>
                <li>Rank: {details.is_staff ? 'Staff' : 'User'}</li>
                <li>Email: {details.email}</li>
                <li>Last login: {new Date(details.last_login).toLocaleString()}</li>
            </ul>
        </Content>
    );
};

/*
 * GET /sign-up
 * POST /sign-up
 */
export const UserSignUpView = ({title, queryParameters}) => {
    const isSuccess = 'success' in queryParameters;
    const isError = 'error' in queryParameters;

    const {status} = state => state.user.signUp.__meta__;

    return (
        <Content status={status} title={title} ignoreBadRequest>
            <UserSignUp isSuccess={isSuccess} isError={isError} />
        </Content>
    );
};

UserSignUpView.willDispatchEvents = (request, response, urlProperties, state, dispatch) => {
    if (request.method === 'POST') {
        return submitServerSignUp(request, response, dispatch);
    }
};

/*
 * GET /sign-in
 * POST /sign-in
 */
export const UserSignInView = ({title, queryParameters, state}) => {
    const isError = 'error' in queryParameters;

    const {status} = state.user.signInOut.__meta__;

    return (
        <Content status={status} title={title} ignoreBadRequest>
            <UserSignIn isError={isError} />
        </Content>
    );
};

UserSignInView.willDispatchEvents = (request, response, urlProperties, state, dispatch) => {
    if (request.method === 'POST') {
        return submitServerSignIn(request, response, dispatch);
    }
};

/*
 * POST /sign-out
 */
export const UserSignOutView = () => <Redirect to="/" />;

UserSignOutView.willDispatchEvents = (request, response, urlProperties, state, dispatch) => {
    if (request.method === 'POST') {
        return submitServerSignOut(request, response, dispatch);
    }
};
