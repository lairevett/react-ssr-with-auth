import {ACCESS, registerUrl} from '../app/utils/router';
import {UserSignUpView, UserSignInView, UserSignOutView, UserProfileView} from './views';

registerUrl('userAdmin', '/admin', ['get'], () => null, ACCESS.REQUIRE_ADMIN, {title: 'Admin'});

registerUrl('userProfile', '/profile', ['get'], UserProfileView, ACCESS.REQUIRE_AUTH, {title: 'Profile'});

registerUrl('userSignUp', '/sign-up', ['get', 'post'], UserSignUpView, ACCESS.DISALLOW_AUTH, {
    title: 'Sign up',
});

registerUrl('userSignIn', '/sign-in', ['get', 'post'], UserSignInView, ACCESS.DISALLOW_AUTH, {
    title: 'Sign in',
});

registerUrl('userSignOut', '/sign-out', ['get', 'post'], UserSignOutView, ACCESS.REQUIRE_AUTH);
