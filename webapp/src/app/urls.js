import {ACCESS, registerUrl} from './utils/router';

import {IndexView} from './views';
import {callbackGetDispatchEvents} from './utils/app';

registerUrl('appIndex', '/', ['get'], IndexView, ACCESS.ALLOW_ANY, {title: 'Homepage', callbackGetDispatchEvents});
