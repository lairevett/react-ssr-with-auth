import {useDispatch, useSelector} from 'react-redux';
import {useEffect} from 'react';
import {makeClientHandleNextDispatches} from '../../__server__/actions';

export const makeDispatchEvents = (onInit = [], onDestroy = [], dependencies = []) => ({
    onInit,
    onDestroy,
    dependencies,
});

export const useClientDispatchEvents = dispatchEvents => {
    const willServerDispatchNextEvents = useSelector(state => state.__server__.willDispatchNextEvents);
    const dispatch = useDispatch();
    const {onInit, onDestroy} = dispatchEvents;

    useEffect(() => {
        // The first dispatches are made server-side, no need to call onInit again client-side.
        dispatch(makeClientHandleNextDispatches());
        if (willServerDispatchNextEvents) return;

        const unwrappedOnInitEvents = onInit.map(onInitEvent => onInitEvent());
        // noinspection JSValidateTypes
        Promise.allSettled(unwrappedOnInitEvents)
            .then(promises => promises.forEach(promise => dispatch(promise.value)))
            .catch(error => console.error(error));

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [dispatch, willServerDispatchNextEvents, ...dispatchEvents.dependencies]);

    useEffect(() => {
        return () => {
            const unwrappedOnDestroyEvents = onDestroy.map(onDestroyEvent => onDestroyEvent());
            // noinspection JSValidateTypes
            Promise.allSettled(unwrappedOnDestroyEvents)
                .then(promises => promises.forEach(promise => dispatch(promise.value)))
                .catch(error => console.error(error));
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [dispatch, ...dispatchEvents.dependencies]);
};
