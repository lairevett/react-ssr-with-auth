import {makeDispatchEvents} from './dispatch_events';

export const callbackGetDispatchEvents = () => () =>
    makeDispatchEvents(
        [
            /* any redux action dispatches needed on component mount */
        ],
        [
            /* any redux action dispatches needed on component unmount */
        ],
        [
            /* any dependencies for useEffect */
        ]
    );
