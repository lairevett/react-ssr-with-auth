// Import all of the app urls here.
import '../urls';
import '../../user/urls';

// Needed only if some pages are fetched from database on the go:
// export const registerDynamicUrls = async pageList => {
    // try {
    //     const urls = await import('../../pages/urls');
    //     urls.registerPagesUrls(pageList);
    // } catch (error) {
    //     console.error(error);
    // }
// };
