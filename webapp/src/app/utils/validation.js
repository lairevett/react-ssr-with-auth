export const validateFieldRequired = (field, message = 'Field is required.') => {
    if (
        typeof field === 'undefined' ||
        (typeof field === 'string' && field.replace(/ /g, '').length === 0) ||
        (typeof field === 'boolean' && !field)
    ) {
        return message;
    }

    return null;
};

export const validateFieldContainsNoWhitespace = (field, message = 'Field cannot contain any spaces.') => {
    if (typeof field !== 'undefined' && / /g.test(field)) {
        return message;
    }

    return null;
};

export const validateFieldMinLength = (field, min, customMessage = null) => {
    if (
        (typeof field === 'string' && field.replace(/ /g, '').length === 0) ||
        (typeof field !== 'undefined' && field.length < min)
    ) {
        if (customMessage) return customMessage;
        return `Field value must be at least ${min} characters long.`
    }
    return null;
};

export const validateFieldMaxLength = (field, max, customMessage = null) => {
    if (
        (typeof field === 'string' && field.replace(/ /g, '').length === 0) ||
        (typeof field !== 'undefined' && field.length > max)
    ) {
        if (customMessage) return customMessage;
        return `Field value must be shorter than ${max} characters.`;
    }
    return null;
};

export const validateFieldEquals = (field, value, message = 'Fields must contain the same values.') => {
    if (typeof field !== 'undefined' && field !== value) {
        return message;
    }

    return null;
};

export const validateFieldEmail = email => {
    // Some regex I shamelessly stole from stackoverflow to prevent some of unnecessary calls to backend with invalid emails.
    if (
        typeof email !== 'undefined' &&
        // eslint-disable-next-line security/detect-unsafe-regex
        /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(
            email
        )
    ) {
        return 'Email address must be valid.';
    }

    return null;
};

export const validateUsername = username =>
    validateFieldRequired(username) ||
    validateFieldContainsNoWhitespace(username) ||
    validateFieldMinLength(username, 3) ||
    validateFieldMaxLength(username, 16);

export const validateEmail = email =>
    validateFieldRequired(email) ||
    validateFieldContainsNoWhitespace(email) ||
    validateFieldMaxLength(email, 100) ||
    validateFieldEmail(email);

export const validatePassword = password =>
    validateFieldRequired(password) ||
    validateFieldContainsNoWhitespace(password) ||
    validateFieldMinLength(password, 8) ||
    validateFieldMaxLength(password, 100);
