export const handleFileChange = handler => ({target: {files}}) =>
    handler(files.length > 0 ? {file: files[0], name: files[0].name} : {});

// For redux-forms with file fields.
export const universalFileAppend = (formData, key, value) => {
    const file = value?.file || value?.[0];

    if (file?.buffer !== undefined) {
        formData.append(key, file.buffer, {
            filename: file.originalname,
            contentType: file.mimetype,
            filepath: file.path,
            knownLength: file.size,
        });
    } else {
        formData.append(key, file);
    }
};
