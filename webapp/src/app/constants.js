export const APP_NAME = 'React SSR';
export const APP_DESCRIPTION = 'simple server-side-rendered react app with authentication.';
export const APP_AUTHOR = 'Marcin Sadowski';

export const APP_AUTH_COOKIE_NAME = '_at';
export const APP_CSRF_HEADER_NAME = 'X-CSRF-Token';
export const APP_CSRF_COOKIE_NAME = '_ct';

// noinspection SpellCheckingInspection
export const AUTH_REQUIRED_ROUTE_REDIRECT_URL = '/sign-in';
export const AUTH_DISALLOWED_ROUTE_REDIRECT_URL = '/';
export const ADMIN_REQUIRED_ROUTE_REDIRECT_URL = '/';
