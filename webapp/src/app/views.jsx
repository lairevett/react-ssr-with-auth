/* eslint-disable react/prop-types */
import React from 'react';

import Content from './templates/__partials__/content';
import {STATUS} from './utils/api/constants';

/*
 * GET /
 */
export const IndexView = ({title}) => (
    <Content status={STATUS.OK} title={title}>
        Hello world.
    </Content>
);
