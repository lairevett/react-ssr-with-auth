/* eslint-disable jsx-a11y/no-noninteractive-tabindex */
// Without the tabIndex="0" client and server DOMs aren't synchronized, hydrate() adds it for some reason implicitly.
import {Link} from 'react-router-dom';

import React, {memo} from 'react';
import {useSelector} from 'react-redux';
import {APP_NAME} from '../../constants';
import {retrieveUrl} from '../../utils/router';
import UserSignOutForm from '../../../user/forms/sign_out';

const NavBar = memo(() => {
    const isServerRendered = useSelector(state => state.__server__.willDispatchNextEvents);
    const isAuthenticated = useSelector(state => state.user.signInOut.is_authenticated);
    const user = useSelector(state => state.user.details);

    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <div className="container">
                <Link to={retrieveUrl('appIndex').path} className="navbar-brand">
                    {APP_NAME}, <span className="text-muted">DOM state: {isServerRendered ? 'server' : 'client'}-rendered</span>
                </Link>
                <div className="d-flex">
                    <div className="dropdown">
                        <button
                            className="btn btn-default dropdown-toggle"
                            type="button"
                            id="dropdownMenuButton"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false"
                            aria-label="Account"
                        >
                            <noscript>Account</noscript>
                            <i className="icon-user only-js-inline" />
                        </button>
                        <div
                            className="dropdown-menu dropdown-menu-right"
                            aria-labelledby="dropdownMenuButton"
                            tabIndex="0"
                        >
                            {!isAuthenticated ? (
                                <>
                                    <Link to={retrieveUrl('userSignIn').path} className="dropdown-item">
                                        {retrieveUrl('userSignIn').properties.title}
                                    </Link>
                                    <Link to={retrieveUrl('userSignUp').path} className="dropdown-item">
                                        {retrieveUrl('userSignUp').properties.title}
                                    </Link>
                                </>
                            ) : (
                                <>
                                    <h6 className="dropdown-header">{user.username}</h6>
                                    {user.is_staff && (
                                        <Link to={retrieveUrl('userAdmin').path} className="dropdown-item">
                                            Admin
                                        </Link>
                                    )}
                                    <Link to={retrieveUrl('userProfile').path} className="dropdown-item">
                                        {retrieveUrl('userProfile').properties.title}
                                    </Link>
                                    <div className="dropdown-divider" />
                                    <UserSignOutForm />
                                </>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    );
});

export default NavBar;
