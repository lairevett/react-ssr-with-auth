import React from 'react';
import {APP_AUTHOR} from '../../constants';

const Footer = () => (
    <footer style={{clear: 'both'}} className="footer mt-5 bg-light text-dark shadow-lg p-3">
        <div className="container text-right">
            Copyright &copy; 2020 <strong>{APP_AUTHOR}</strong>
        </div>
    </footer>
);

export default Footer;
