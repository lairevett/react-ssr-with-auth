import PropTypes from 'prop-types';
import React from 'react';

const ErrorAlert = ({message, children}) => (
    <div className="alert alert-danger" role="alert">
        {children ?? message ?? 'Check if all the fields are filled out properly and try again.'}
    </div>
);

ErrorAlert.propTypes = {
    message: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
};

ErrorAlert.defaultProps = {
    message: null,
    children: null,
};

export default ErrorAlert;
