import {combineReducers} from 'redux';
import {reducer as formReducer} from 'redux-form';
import userReducer from '../user/reducer';
import __server__Reducer from '../__server__/reducer';

const reducers = {
    form: formReducer,
    user: userReducer,
    __server__: __server__Reducer,
};

const makeAppReducer = () => combineReducers(reducers);

export default makeAppReducer;
