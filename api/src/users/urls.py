from django.urls import path
from .views import sign_up, sign_in, retrieve_user, sign_out

urlpatterns = [
    path('sign-up/', sign_up),
    path('sign-in/', sign_in),
    path('retrieve-user/', retrieve_user),
    path('sign-out/', sign_out),
]
