from django.conf import settings
from django.contrib.auth import authenticate
from django.contrib.auth.hashers import make_password
from django.middleware.csrf import rotate_token as rotate_csrf_token
from django.views.decorators.csrf import csrf_protect
from rest_framework import permissions, status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from .serializers import UserSerializer
from .utils import get_authentication_token


@api_view(['GET'])
@permission_classes([permissions.IsAuthenticated])
def retrieve_user(request):
    serializer = UserSerializer(request.user)
    return Response(serializer.data)


@api_view(['POST'])
@permission_classes([permissions.AllowAny])
@csrf_protect
def sign_up(request):
    data = {
        'username': request.data.get('username'),
        'email': request.data.get('email'),
        'password': request.data.get('password')
    }

    serializer = UserSerializer(data=data)
    serializer.is_valid(raise_exception=True)
    serializer.save(password=make_password(data['password']))

    return Response(serializer.data, status=status.HTTP_201_CREATED)


@api_view(['POST'])
@permission_classes([permissions.AllowAny])
@csrf_protect
def sign_in(request):
    username = request.data.get('username')
    password = request.data.get('password')
    if not username or not password:
        raise ValidationError('You have to provide both username and password.')

    user = authenticate(username=username, password=password)
    if not user:
        raise ValidationError('Invalid credentials.')

    response = Response({'is_authenticated': True})

    response.set_cookie(settings.CSRF_COOKIE_NAME, rotate_csrf_token(request), samesite='Lax')
    response.set_cookie(settings.AUTH_COOKIE_NAME, get_authentication_token(user), httponly=True, samesite='Lax')

    return response


@api_view(['DELETE'])
@permission_classes([permissions.IsAuthenticated])
@csrf_protect
def sign_out(request):
    response = Response({'is_authenticated': False})
    response.delete_cookie(settings.AUTH_COOKIE_NAME)
    return response
