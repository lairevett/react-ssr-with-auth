from django.contrib.auth.models import User
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'

    def get_fields(self):
        fields = super().get_fields()
        fields.pop('password')

        return fields
