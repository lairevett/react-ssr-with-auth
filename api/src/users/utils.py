from django.conf import settings
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token


class HttpOnlyCookieTokenAuthentication(TokenAuthentication):

    def authenticate(self, request):
        token_name = settings.AUTH_COOKIE_NAME
        if token_name in request.COOKIES and 'HTTP_AUTHORIZATION' not in request.META:
            return self.authenticate_credentials(request.COOKIES[token_name])

        return super().authenticate(request)


def get_authentication_token(user):
    token, _ = Token.objects.get_or_create(user=user)
    return token.key
